package finergit.ast.token;

public class AMODIFIER extends JavaToken {
    public AMODIFIER(String name) {
        super("ModifierToken[" + name + "]" );
    }
}