package finergit.ast.token;

public class APARAMETER extends JavaToken {
    public APARAMETER(String name) {
        super("ParameterToken[" + name + "]" );
    }
}