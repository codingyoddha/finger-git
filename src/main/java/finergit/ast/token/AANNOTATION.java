package finergit.ast.token;

public class AANNOTATION extends JavaToken {
    public AANNOTATION(String name) {
        super("AnnotationToken[" + name + "]" );
    }
}