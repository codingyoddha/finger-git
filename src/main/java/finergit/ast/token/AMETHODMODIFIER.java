package finergit.ast.token;

public class AMETHODMODIFIER extends JavaToken {
    public AMETHODMODIFIER(String name) {
        super("MethodModifierToken[" + name + "]" );
    }
}