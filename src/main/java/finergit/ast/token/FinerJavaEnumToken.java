package finergit.ast.token;

import finergit.ast.FinerJavaEnum;
import finergit.ast.FinerJavaInterface;

public class FinerJavaEnumToken extends JavaToken {

  final FinerJavaEnum finerJavaClass;

  public FinerJavaEnumToken(final String value, final FinerJavaEnum finerJavaClass) {
    super(value);
    this.finerJavaClass = finerJavaClass;
  }
}
