package finergit.ast.token;

public class APACKAGE extends JavaToken {
    public APACKAGE(String name) {
        super("PackageToken[" + name + "]");
    }

}