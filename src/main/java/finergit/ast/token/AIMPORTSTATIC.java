package finergit.ast.token;

public class AIMPORTSTATIC extends JavaToken {
    public AIMPORTSTATIC(String name) {
        super("ImportStaticToken[" + name + "]");
    }

}