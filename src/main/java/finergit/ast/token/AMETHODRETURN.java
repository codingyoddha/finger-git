package finergit.ast.token;

public class AMETHODRETURN extends JavaToken {

    public AMETHODRETURN(String name) {
        super("MethodReturnToken[" + name + "]");
    }
}
