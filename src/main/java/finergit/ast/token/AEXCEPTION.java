package finergit.ast.token;

public class AEXCEPTION extends JavaToken {
    public AEXCEPTION(String name) {
        super("ExceptionToken[" + name + "]");
    }

}