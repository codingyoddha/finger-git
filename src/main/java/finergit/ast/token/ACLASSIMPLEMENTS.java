package finergit.ast.token;

public class ACLASSIMPLEMENTS extends JavaToken {
    public ACLASSIMPLEMENTS(String name) {
        super("ClassImplementsToken[" + name + "]");
    }

}