package finergit.ast.token;

public class ACLASSEXTENDS extends JavaToken {
    public ACLASSEXTENDS(String name) {
        super("ClassExtendsToken[" + name + "]");
    }

}