package finergit.ast.token;

public class AMETHODNAME extends JavaToken {
    public AMETHODNAME(String name) {
        super("MethodNameToken[" + name + "]");
    }

}