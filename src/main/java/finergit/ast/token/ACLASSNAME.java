package finergit.ast.token;

public class ACLASSNAME extends JavaToken {
    public ACLASSNAME(String name) {
        super("ClassNameToken[" + name + "]");
    }

}