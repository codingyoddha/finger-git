package finergit.ast.token;

import finergit.ast.FinerJavaClass;
import finergit.ast.FinerJavaInterface;

public class FinerJavaInterfaceToken extends JavaToken {

  final FinerJavaInterface finerJavaClass;

  public FinerJavaInterfaceToken(final String value, final FinerJavaInterface finerJavaClass) {
    super(value);
    this.finerJavaClass = finerJavaClass;
  }
}
