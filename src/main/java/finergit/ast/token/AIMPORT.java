package finergit.ast.token;

public class AIMPORT extends JavaToken {
    public AIMPORT(String name) {
        super("ImportToken[" + name + "]");
    }

}