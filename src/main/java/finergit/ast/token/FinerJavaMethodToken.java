package finergit.ast.token;

import finergit.ast.FinerJavaModule;

public class FinerJavaMethodToken extends JavaToken {

  FinerJavaModule finerJavaMethod;

  public FinerJavaMethodToken(final String value, final FinerJavaModule finerJavaMethod) {
    super(value);
    this.finerJavaMethod = this.finerJavaMethod;
  }
}
