package finergit.ast;

import finergit.FinerGitConfig;
import finergit.ast.FinerJavaModule;

import java.nio.file.Path;

public class FinerJavaConstructor extends FinerJavaModule {

  private static final String METHOD_EXTENSION = ".cmjava";
  private static final String METHOD_DELIMITER = "#";

  public FinerJavaConstructor(final String name, final FinerJavaModule outerModule,
                              final FinerGitConfig config) {
    super(name, outerModule, config);
  }

  @Override
  public Path getDirectory() {
    return this.outerModule.getDirectory();
  }

  @Override
  public String getExtension() {
    return METHOD_EXTENSION;
  }

  /**
   * ベースネーム（拡張子がないファイル名）を返す．
   *
   * @return
   */
  @Override
  public String getBaseName() {
    return this.outerModule.getBaseName() + METHOD_DELIMITER + this.name;
  }
}
