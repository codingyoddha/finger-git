# FinerGit 
[Click here to Japanese README](#user-content-finergit-日本語)

FinerGit is a tool to easily obtain change histories of Java methods by using the Git mechanism.
FinerGit takes a Git repository as its input and generates another Git repository.
Git repositories that FinerGit generates have the following features.
- Every method in source files gets extracted as a single file.
- Every line of extracted files includes only a single token.

The first feature realizes that Java methods are able to be tracked with Git mechanism.
The second feature improves the trackability of Java methods.


## Quick start

### Check your environment
First of all, please make sure that Java 11+ is installed in your environment.
```shell-session
$ java -version
java version "11.0.6" 2020-01-14 LTS
Java(TM) SE Runtime Environment 18.9 (build 11.0.6+8-LTS)
Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.6+8-LTS, mixed mode)
```

### Build FinerGit
Build FinerGit.jar with the following commands.
```shell-session
$ git clone https://github.com/kusumotolab/FinerGit.git
$ cd FinerGit
$ ./gradlew shadowJar
```

It is fine if you have `FinerGit-all.jar` in the `FinerGit/build/lib` directory.
If you specify `jar` or `build` instead of `shadowJar` as an argument for `gradlew`, you will get `FinerGit.jar` in the directory. 
`FinerGit.jar` is not a single executable jar file.

### Run FinerGit
A basic command to convert a Git repository to a FinerGit repository is as follows.
```shell-session
$ java -jar FinerGit-all.jar create --src /path/to/repoA --des /path/to/repoB
```
Herein, `/path/to/repoA` is an existing Git repository, and `/path/to/repoB` is a path to output a new FinerGit repository.

FinerGit has several options for converting repositories.
The options are printed with the following command.
```shell-session
$ java -jar build/libs/FinerGit-all.jar create
```

### See change histories of Java methods in a FinerGit repository

In FinerGit repositories, there are files whose extensions are `.cjava`, `fjava`, or `.mjava`.

- Extension `.cjava` means that its file represents a Java class. But all methods included in the class get extracted as different files.
- Extension `.fjava` means that its file represents a Java field. Names of method files follow the format of `ClassName#FieldName.fjava`.
- Extension `.mjava` means that its file represents a Java method. Names of method files follow the format of `ClassName#MethodSignature.mjava`.

If you want to see the change history of `Foo#bar().mjava`, type the following command.
```shell-session
$ git log "Foo#bar().mjava"
```
You will get all commits where method `bar()` was changed.

``--follow`` option is useful since it enables Git to track files even if their names got changed.
```shell-session
$ git log --follow "Foo#bar().mjava"
```




## At the end

FinerGit is still under development. We mainly use MacOS + JDK11 + ~~Eclipse~~ IntelliJ IDEA in our FinerGit development.
We rarely test FinerGit on Windows environment.

[cregit](https://github.com/cregit/cregit) and [git-stein](https://github.com/sh5i/git-stein) are other tools that convert/rewrite Git repositories.
FinerGit internally uses git-stein.
